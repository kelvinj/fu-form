<?php

namespace Fu\Form;

/**
 * This renderer forces the author to describe exactly how they want their elements being displayed.
 *
 * @link       http://pear.php.net/package/HTML_QuickForm_Renderer_Tableless
 */
abstract class AbstractRenderer extends \HTML_QuickForm_Renderer_Default
{
    public function __construct()
    {
    }

    abstract public function getTemplateCode($type, $variation = 'default');

    /**
     * Header Template string
     *
     * @var string
     * @access   private
     */
    public $_headerTemplate = "<legend>{header}</legend>";

    /**
     * Element template string
     *
     * @var string
     * @access   private
     */
    public $_elementTemplate = "<label class=\"element\">{begin required}<span class=\"required\">*</span>{end required}{label}</label><div class=\"element{begin error} error{end error}\">{error}<span class=\"error\">{error}</span><br />{end error}{element}</div>";

    /**
     * Form template string
     *
     * @var string
     * @access   private
     */
    public $_formTemplate = "<form{attributes}>\n\t<div style=\"display: none;\">\n{hidden}\t</div>\n{content}\n</form>";

    /**
     * Template used when opening a fieldset
     *
     * @var string
     * @access   private
     */
    public $_openFieldsetTemplate = "<fieldset{id}{attributes}>";

    /**
     * Template used when opening a fieldset without a "header" (<legend>)
     */
    public $_openHiddenFieldsetTemplate = "<fieldset class=\"hidden{class}\">";

    /**
     * Template used when closing a fieldset
     *
     * @var string
     * @access   private
     */
    public $_closeFieldsetTemplate = "</fieldset>";

    /**
     * Required Note template string
     *
     * @var string
     * @access   private
     */
    public $_requiredNoteTemplate = "<span class=\"reqnote\">{requiredNote}</span>";

    /**
     * How many fieldsets are open
     *
     * @var      integer
     * @access   private
     */
    public $_fieldsetsOpen = 0;

    /**
     * Array of element names that indicate the end of a fieldset
     * (a new one will be opened when the next header element occurs)
     *
     * @var array
     * @access   private
     */
    public $_stopFieldsetElements = [];

    /**
     * Stores the name of the form, used for prepending to input ids to avoid conflicts over multiple forms
     */
    public $_formName;

    /**
     * Called when visiting a header element
     *
     * @param object     An HTML_QuickForm_header element being visited
     * @access   public
     * @return void
     */
    public function renderHeader(&$header)
    {
        $name = $header->getName();
        $id = empty($name) ? '' : ' id="'.$name.'"';
        if (!empty($name) && isset($this->_templates[$name])) {
            $header_html = str_replace('{header}', $header->toHtml(), $this->_templates[$name]);
        } else {
            $header_html = str_replace('{header}', $header->toHtml(), $this->_headerTemplate);
        }
        $attributes = $header->getAttributes();
        $strAttr = '';
        if (is_array($attributes)) {
            $charset = \HTML_Common::charset();
            foreach ($attributes as $key => $value) {
                if ($key == 'name') {
                    continue;
                }
                $strAttr .= ' '.$key.'="'.htmlspecialchars($value, ENT_COMPAT, $charset).'"';
            }
        }
        if ($this->_fieldsetsOpen > 0) {
            $this->_html .= $this->_closeFieldsetTemplate;
            $this->_fieldsetsOpen--;
        }
        $openFieldsetTemplate = str_replace('{id}', $id, $this->_openFieldsetTemplate);
        $openFieldsetTemplate = str_replace(
            '{attributes}',
            $strAttr,
            $openFieldsetTemplate
        );
        $this->_html .= $openFieldsetTemplate.$header_html;
        $this->_fieldsetsOpen++;
    }

    /**
     * Renders an element Html
     * Called when visiting an element
     *
     * @param object     An HTML_QuickForm_element object being visited
     * @param bool       Whether an element is required
     * @param string     An error message associated with an element
     * @access public
     * @return void
     */
    public function renderElement(&$element, $required, $error)
    {
        $this->_handleStopFieldsetElements($element->getName());
        if (!$this->_inGroup) {
            $html = $this->_prepareTemplate($element->getName(), $element->getLabel(), $required, $error);
            // the following lines (until the "elseif") were changed / added
            // compared to the default renderer
            if (!is_null($element->getAttribute('id'))) {
                $id = $element->getAttribute('id');
            } else {
                $id = $this->_formName.'_'.$element->getName();
                $element->setAttribute('id', $id);
            }

            $element_html = $element->toHtml();

            if ($element->getType() != 'static' && !empty($id)) {
                $html = str_replace('<label', '<label for="'.$id.'"', $html);
                $element_html = preg_replace(
                    '/'.preg_quote('name="'.$id).'/',
                    'id="'.$id.'" name="'.$id,
                    $element_html,
                    1
                );
            }
            $this->_html .= str_replace('{element}', $element_html, $html);
        } elseif (!empty($this->_groupElementTemplate)) {
            $id = $element->getAttribute('id');
            $html = str_replace('{label}', $element->getLabel(), $this->_groupElementTemplate);
            if ($required) {
                $html = str_replace('{begin required}', '', $html);
                $html = str_replace('{end required}', '', $html);
            } else {
                $html = preg_replace("/([ \t\n\r]*)?{begin required}(\s|\S)*{end required}([ \t\n\r]*)?/i", '', $html);
            }
            $html = str_replace('<label', '<label for="'.$id.'"', $html);
            $this->_groupElements[] = str_replace('{element}', $element->toHtml(), $html);
        } else {
            $this->_groupElements[] = $element->toHtml();
        }
    }

    /**
     * Helper method for renderElement
     *
     * @param string      Element name
     * @param mixed       Element label (if using an array of labels, you should set the appropriate template)
     * @param bool        Whether an element is required
     * @param string      Error message associated with the element
     * @access   private
     * @return string Html for element
     * @see      renderElement()
     */
    public function _prepareTemplate($name, $label, $required, $error)
    {
        if (is_array($label)) {
            $nameLabel = array_shift($label);
        } else {
            $nameLabel = $label;
        }
        if (isset($this->_templates[$name])) {
            $html = str_replace('{label}', $nameLabel, $this->_templates[$name]);
        } else {
            $html = str_replace('{label}', $nameLabel, $this->_elementTemplate);
        }
        if ($required) {
            $html = str_replace('{begin required}', '', $html);
            $html = str_replace('{end required}', '', $html);
        } else {
            $html = preg_replace("/([ \t\n\r]*)?{begin required}.*{end required}([ \t\n\r]*)?/isU", '', $html);
        }
        if (isset($error)) {
            $html = str_replace('{error}', $error, $html);
            $html = str_replace('{begin error}', '', $html);
            $html = str_replace('{end error}', '', $html);
        } else {
            $html = preg_replace("/([ \t\n\r]*)?{begin error}.*{end error}([ \t\n\r]*)?/isU", '', $html);
        }

        if (is_array($label)) {
            foreach ($label as $key => $text) {
                $key = is_int($key) ? $key + 2 : $key;
                $html = str_replace("{label_{$key}}", $text, $html);
                $html = str_replace("{begin label_{$key}}", '', $html);
                $html = str_replace("{end label_{$key}}", '', $html);
            }
        }
        if (strpos($html, '{label_')) {
            $html = preg_replace('/\s*{begin label_(\S+)}.*{end label_\1}\s*/is', '', $html);
        }

        return $html;
    }


    /**
     * Renders an hidden element
     * Called when visiting a hidden element
     *
     * @param $element
     * @param $required
     * @param $error
     * @return void
     */
    public function renderHidden(&$element, $required, $error)
    {
        if (!is_null($element->getAttribute('id'))) {
            $id = $element->getAttribute('id');
        } else {
            $id = $element->getName();
        }
        $html = $element->toHtml();
        if (!empty($id)) {
            $html = str_replace(
                'name="'.$id,
                'id="'.$id.'" name="'.$id,
                $html
            );
        }
        $this->_hiddenHtml .= $html."\n";
    }

    /**
     * Called when visiting a group, before processing any group elements
     *
     * @param object     An HTML_QuickForm_group object being visited
     * @param bool       Whether a group is required
     * @param string     An error message associated with a group
     * @access public
     * @return void
     */
    public function startGroup(&$group, $required, $error)
    {
        $this->_handleStopFieldsetElements($group->getName());
        parent::startGroup($group, $required, $error);
    }

    /**
     * Called when visiting a group, after processing all group elements
     *
     * @param object      An HTML_QuickForm_group object being visited
     * @access   public
     * @return void
     */
    public function finishGroup(&$group)
    {
        $separator = $group->_separator;
        if (is_array($separator)) {
            $count = count($separator);
            $html = '';
            for ($i = 0; $i < count($this->_groupElements); $i++) {
                $html .= (0 == $i ? '' : $separator[($i - 1) % $count]).$this->_groupElements[$i];
            }
        } else {
            if (is_null($separator)) {
                $separator = '&nbsp;';
            }
            $html = implode((string)$separator, $this->_groupElements);
        }
        if (!empty($this->_groupWrap)) {
            $html = str_replace('{content}', $html, $this->_groupWrap);
        }
        if (!is_null($group->getAttribute('id'))) {
            $id = $group->getAttribute('id');
        } else {
            $id = $group->getName();
        }
        $groupTemplate = $this->_groupTemplate;

        $this->_html .= str_replace('{element}', $html, $groupTemplate);
        $this->_inGroup = false;
    }

    /**
     * Called when visiting a form, before processing any form elements
     *
     * @param object      An HTML_QuickForm object being visited
     * @access   public
     * @return void
     */
    public function startForm(&$form)
    {
        $this->_formName = $form->_attributes['id'] ? $form->_attributes['id'] : $form->_attributes['name'];
        $this->_fieldsetsOpen = 0;
        parent::startForm($form);
    }

    /**
     * Called when visiting a form, after processing all form elements
     * Adds required note, form attributes, validation javascript and form content.
     *
     * @param object      An HTML_QuickForm object being visited
     * @access   public
     * @return void
     */
    public function finishForm(&$form)
    {
        // add a required note, if one is needed
        if (!empty($form->_required) && !$form->_freezeAll) {
            $requiredNote = $form->getRequiredNote();
            // replace default required note by DOM/XHTML optimized note
            if ($requiredNote == '<span style="font-size:80%; color:#ff0000;">*</span><span style="font-size:80%;"> denotes required field</span>') {
                $requiredNote = '<span class="required">*</span> denotes required field';
            }
            $this->_html .= str_replace('{requiredNote}', $requiredNote, $this->_requiredNoteTemplate);
        }
        // close the open fieldset
        if ($this->_fieldsetsOpen > 0) {
            $this->_html .= $this->_closeFieldsetTemplate;
            $this->_fieldsetsOpen--;
        }
        // add form attributes and content
        $html = str_replace('{attributes}', $form->getAttributes(true), $this->_formTemplate);
        if (strpos($this->_formTemplate, '{hidden}')) {
            $html = str_replace('{hidden}', $this->_hiddenHtml, $html);
        } else {
            $this->_html .= $this->_hiddenHtml;
        }
        $this->_hiddenHtml = '';
        $this->_html = str_replace('{content}', $this->_html, $html);
        $this->_html = str_replace('></label>', '>&nbsp;</label>', $this->_html);
        // add a validation script
        if ('' != ($script = $form->getValidationScript())) {
            $this->_html = $script."\n".$this->_html;
        }
    }

    /**
     * Sets the template used when opening a fieldset
     *
     * @param string      The HTML used when opening a fieldset
     * @access      public
     * @return void
     */
    public function setOpenFieldsetTemplate($html)
    {
        $this->_openFieldsetTemplate = $html;
    }

    /**
     * Sets the template used when opening a hidden fieldset
     * (i.e. a fieldset that is opened when there is no header element)
     *
     * @param string      The HTML used when opening a hidden fieldset
     * @access      public
     * @return void
     */
    public function setOpenHiddenFieldsetTemplate($html)
    {
        $this->_openHiddenFieldsetTemplate = $html;
    }

    /**
     * Sets the template used when closing a fieldset
     *
     * @param string      The HTML used when closing a fieldset
     * @access      public
     * @return void
     */
    public function setCloseFieldsetTemplate($html)
    {
        $this->_closeFieldsetTemplate = $html;
    }

    /**
     * Adds one or more element names that indicate the end of a fieldset
     * (a new one will be opened when a the next header element occurs)
     *
     * @param mixed $element
     * @param string $class
     * @access public
     * @return void
     */
    public function addStopFieldsetElements($element, $class = '')
    {
        if (is_array($element)) {
            $elements = [];
            foreach ($element as $name) {
                $elements[$name] = $class;
            }
            $this->_stopFieldsetElements = array_merge(
                $this->_stopFieldsetElements,
                $elements
            );
        } else {
            $this->_stopFieldsetElements[$element] = $class;
        }
    }

    /**
     * Handle element/group names that indicate the end of a group
     *
     * @param string     The name of the element or group
     * @access private
     * @return void
     */
    public function _handleStopFieldsetElements($element)
    {
        // if the element/group name indicates the end of a fieldset, close
        // the fieldset
        if (array_key_exists($element, $this->_stopFieldsetElements)
            && $this->_fieldsetsOpen > 0
        ) {
            $this->_html .= $this->_closeFieldsetTemplate;
            $this->_fieldsetsOpen--;
        }

        // if no fieldset was opened, we need to open a hidden one here to get
        // XHTML validity
        if ($this->_fieldsetsOpen === 0) {
            $replace = '';
            if (array_key_exists($element, $this->_stopFieldsetElements)
                && $this->_stopFieldsetElements[$element] != ''
            ) {
                $replace = ' '.$this->_stopFieldsetElements[$element];
            }
            $this->_html .= str_replace(
                '{class}',
                $replace,
                $this->_openHiddenFieldsetTemplate
            );
            $this->_fieldsetsOpen++;
        }
    }

    /**
     * Sets element template
     *
     * @param string    The HTML surrounding an element
     * @param mixed     (optional) Name(s) of the element to apply template
     *                    for (either single element name as string or multiple
     *                    element names as an array)
     * @access  public
     * @return void
     */
    public function setElementTemplate($html, $element = null)
    {
        if (is_null($element)) {
            $this->_elementTemplate = $html;
        } elseif (is_array($element)) {
            foreach ($element as $name) {
                $this->_templates[$name] = $html;
            }
        } else {
            $this->_templates[$element] = $html;
        }
    }
}
