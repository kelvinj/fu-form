<?php
namespace Fu\Form;

class Renderer extends AbstractRenderer
{
    public $forms = array(
      'default' => '<form{attributes}>{hidden}{content}</form>',
    );

    public $open_fieldsets = array(
      'default' => '<fieldset{id}{attributes}>',
    );

    public $open_hidden_fieldsets = array(
      'default' => '<fieldset>',
    );

    public $close_fieldsets = array(
      'default' => '</fieldset>',
    );

    public $headers = array(
      'default' => '<legend>{header}</legend>',
    );

   /*
      Used for multiple checkboxes or radio buttons, defined using 'element_template'
   */
    public $groups = array(
      'checkbox' => '<label class="checkbox">{element} {label}</label>',
      'inline_checkbox' => '<label class="checkbox inline">{element} {label}</label>',
      'radio' => '<label class="radio">{element} {label}</label>',
      'inline_radio' => '<label class="radio inline">{element} {label}</label>',
    );

    public $elements = array(
      'default' => '
		 <div class="control {begin error}error{end error}">
			<label>{label}</label> {begin required}<span class="req">*</span>{end required}
			{element}
			{begin label_2}
			<p class="note">{label_2}</p>
			{end label_2}
		 </div>
		 ',

      'table_default' => '<span class="{begin error}error{end error}">{element}</span>',

      'nolabel' => '
		 <div class="control">
			{element} {begin required}<span class="req">*</span>{end required}
			{begin label_2}
			<p class="note">{label_2}</p>
			{end label_2}
		 </div>
		 ',

      'checkbox' => '
		 <div class="control checkbox {begin error}error{end error}">
			<label class="checkbox">{element} {label}</label>
			{begin label_2}
			<p class="note">{label_2}</p>
			{end label_2}
		 </div>
		 ',

      'multiple' => '
		 <div class="control group {begin error}error{end error}">
			<label>{label} {begin required}<span class="req">*</span>{end required}</label>
			<div class="controls block">
			   {element}
			</div>
			{begin label_2}
			<p class="note">{label_2}</p>
			{end label_2}
		 </div>
		 ',

      'multiple_horizontal' => '
		 <div class="control group {begin error}error{end error}">
			<label>{label} {begin required}<span class="req">*</span>{end required}</label>
			<div class="controls inline">
			   {element}
			</div>
			{begin label_2}
			<p class="note">{label_2}</p>
			{end label_2}
		 </div>
		 ',

      'submit' => '<div class="form-actions">{element}</div>',
      'submit_with_save' => '<div class="form-actions"><input type="submit" value="Save for later" name="__save" class=""/> {element}</div>',

    );

    public $html = array(
      'form_note' => '<p>%s</p>',
    );

    public $required_notes = array(
      'default' => '&nbsp;',
    );

    public function __construct()
    {
        parent::__construct();

        parent::setFormTemplate($this->forms['default']);
        parent::setOpenFieldsetTemplate($this->open_fieldsets['default']);
        parent::setOpenHiddenFieldsetTemplate($this->open_hidden_fieldsets['default']);
        parent::setCloseFieldsetTemplate($this->close_fieldsets['default']);
        parent::setHeaderTemplate($this->headers['default']);
        parent::setElementTemplate($this->elements['default']);
        parent::setRequiredNoteTemplate($this->required_notes['default']);
    }

    public function getTemplateCode($type, $variation = 'default')
    {
        $template = null;
        if (property_exists($this, $type)) {
            if (array_key_exists($variation, $this->{$type})) {
                $template = $this->{$type}[$variation];
            } else {
                $template = $this->{$type}['default'];
            }
        } else {
            $template = $variation;
        }

        return $template;
    }

    /**
     * @param $element
     * @param $required
     * @param $errors
     *
     * See link for why this change is needed https://github.com/flack/quickform/commit/df39b4a54b010b32ab6342fb5df20883b28f2421#diff-5184501931aa83f429b659e1efc28c24
     * The accept() method was removed and that's how the renderHidden() method is called
     */
    public function renderElement(&$element, $required, $errors)
    {
        if ($element->_attributes['type'] == 'hidden') {
            return parent::renderHidden($element, $required, $errors);
        }

        return parent::renderElement($element, $required, $errors);
    }
}
