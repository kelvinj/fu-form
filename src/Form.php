<?php

namespace Fu;

use Fu\Form\AbstractRenderer;
use Fu\Form\Renderer;
use HTML_QuickForm;

/**
 * All site form goodness
 */
class Form extends \ArrayObject
{
    protected $default_options = [
        'name'         => 'name', //string - Form's name.
        'method'       => 'POST', //string - (optional) Form's method
        'action'       => '',  //string - (optional) Form's action
        'target'       => '', //string - (optional) Form's target
        'attributes'   => '', //mixed - (optional) Extra attributes for <form> tag
        'track_submit' => true, //boolean - (optional) Whether to track if the form was submitted by adding a special hidden field. If the name of such field is not present in the $_GET or $_POST values, the form will be considered as not submitted.
        'validation'   => 'server', // server or client

        'fields'         => [], // fields to pass through
        'default_values' => [], // vals for fields

        'form_template'          => '', // can pass through on fly... or will be in Form config (i.e. default)
        'element_template'       => '', // as above
        'checkbox_template'      => '', // as above
        'header_template'        => '', // as above
        'group_template'         => '', // as above
        'required_note_template' => '',
        'required_note'          => '', // as above

        'hidden_fields'     => [],
        'submit_text'       => 'update',
        'submit_name'       => 'submit',
        'submit_attributes' => 'class="button"',
        'submit_template'   => 'default',

        'input_attributes'    => '', // e.g size="50" and/or class="blah"
        'textarea_sizes'      => [
            'small'  => 'rows="3"',
            'medium' => 'rows="6"',
            'large'  => 'rows="9"',
        ],
        'checkbox_attributes' => '',
    ];

    public $form;

    /**
     * Constructor class
     *
     * @param array $options options
     * @param AbstractRenderer $renderer
     * @throws \HTML_QuickForm_Error
     */
    public function __construct(array $options = [], AbstractRenderer $renderer = null)
    {
        if (isset($options['form_name'])) {
            $options['name'] = $options['form_name'];
        }

        if (isset($options['form_attributes'])) {
            $options['attributes'] = $options['form_attributes'];
        }

        if (isset($options['renderer']) && !$renderer) {
            $class = '\\'.$options['renderer'];
            $renderer = new $class;
        }

        $this->options = array_merge($this->default_options, $options);
        $this->form = new HTML_QuickForm(
            $this->options['name'],
            $this->options['method'],
            $this->options['action'],
            $this->options['target'],
            $this->options['attributes'],
            $this->options['track_submit']
        );

        if (is_null($renderer)) {
            $renderer = new Renderer();
        }

        $this->renderer = $renderer;

        $errorReportingLevel = error_reporting(E_ALL ^ E_NOTICE);

        $this->setFormTemplate();
        $this->setElementTemplate();
        $this->setHeaderTemplate();
        $this->setRequiredNoteTemplate();

        $this->addFields();

        if ($this->options['default_values']) {
            $this->form->setDefaults($this->options['default_values']);
        }

        if ($this->options['submit_text']) {
            $submit_name = $this->options['submit_name'];
            if ($this->options['submit_template']) {
                $r = $this->renderer;
                $r->setElementTemplate($r->getTemplateCode('elements', $this->options['submit_template']), $submit_name);
            }
            $this->form->addElement('submit', $submit_name, $this->options['submit_text'], $this->options['submit_attributes']);
        }

        if ($this->options['required_note']) {
            $this->form->setRequiredNote($this->options['required_note']);
        }

        error_reporting($errorReportingLevel);
    }

    public function setOption($k, $v)
    {
        $this->options[$k] = $v;
    }

    /**
     * Output form
     *
     * @param bool $freeze
     */
    public function display($freeze = false)
    {
        echo $this->toHtml($freeze);
    }

    /**
     * Return HTML of form
     *
     * @param bool $freeze
     * @return
     * @throws \HTML_QuickForm_Error
     */
    public function toHtml($freeze = false)
    {
        if ($freeze == true) {
            $this->form->freeze();
        }

        $this->form->accept($this->renderer);

        return $this->renderer->toHtml();
    }

    /**
     * get form template
     **/
    protected function setFormTemplate($template = null)
    {
        if ($template) {
            $r = $this->renderer;
            $r->setFormTemplate($r->getTemplateCode('forms', $template));
        }
    }

    protected function setElementTemplate()
    {
        if ($this->options['element_template']) {
            $r = $this->renderer;
            $r->setElementTemplate($r->getTemplateCode('elements', $this->options['element_template']));
        }
    }

    protected function setOpenFieldsetTemplate($template)
    {
        $r = $this->renderer;
        $r->setOpenFieldsetTemplate(
            $r->getTemplateCode('open_fieldsets', $template)
        );
    }

    protected function setOpenHiddenFieldsetTemplate($template)
    {
        $r = $this->renderer;
        $r->setOpenHiddenFieldsetTemplate(
            $r->getTemplateCode('open_hidden_fieldsets', $template)
        );
    }

    protected function setHeaderTemplate()
    {
        if ($this->options['header_template']) {
            $r = $this->renderer;
            $r->setHeaderTemplate($r->getTemplateCode('headers', $this->options['header_template']));
        }
    }

    protected function setRequiredNoteTemplate()
    {
        if ($this->options['required_note_template']) {
            $r = $this->renderer;
            $r->setRequiredNoteTemplate($r->getTemplateCode('required_notes', $this->options['required_note_template']));
        }
    }

    /**
     * Add fields to form
     */
    private function addFields()
    {
        if (count((array)$this->options['hidden_fields']) > 0) {
            foreach ($this->options['hidden_fields'] as $name => $value) {
                $this->form->addElement('hidden', $name, $value);
            }
        }

        if (count((array)$this->options['fields']) > 0) {
            foreach ($this->options['fields'] as $name => $options) {
                if (!is_numeric($name)) {
                    $options['name'] = $name;
                }
                if (!$options['type']) {
                    $options['type'] = 'input';
                }

                $this->addField($options);
            }
        }
    }

    /**
     * Add field to form
     *
     * @param array $options
     * @throws \HTML_QuickForm_Error
     */
    private function addField(array $options)
    {
        $options['label'] = $options['label'] ?: ucfirst(strtolower(str_replace('_', ' ', $options['name'])));
        $options['name'] = $options['name'] ?: strtolower(str_replace(' ', '_', $options['label']));

        if ($options['attr']) {
            $options['attributes'] = $options['attr'];
        }

        if ($options['note']) {
            $options['label'] = (array)$options['label'];
            $options['label'][] = $options['note'];
        }

        if (isset($options['default'])) {
            if (!isset($this->options['default_values'][$options['name']])) {
                $this->options['default_values'][$options['name']] = $options['default'];
            }
        }

        // for templating, because input elements and fieldsets are treated differently, we have to know what we're dealing with
        $is_header = ($options['type'] == 'fieldset');
        $template_stack = $is_header ? 'headers' : 'elements';

        // we have some HTML to insert before or after the {element} part of the template
        if ($options['before_element'] || $options['after_element'] || $options['before_template'] || $options['after_template']) {
            $element_template = $options['template'] ?: $this->options['element_template'];
            $element_template = $this->renderer->getTemplateCode($template_stack, $element_template);

            if ($options['before_element']) {
                $element_template = str_replace('{element}', $options['before_element'].'{element}', $element_template);
            }

            if ($options['after_element']) {
                $element_template = str_replace('{element}', '{element}'.$options['after_element'], $element_template);
            }

            if ($options['before_template']) {
                $element_template = $options['before_template'].$element_template;
            }

            if ($options['after_template']) {
                $element_template .= $options['after_template'];
            }

            $this->renderer->setElementTemplate($element_template, $options['name']);
        } elseif ($options['template']) {
            $element_template = $this->renderer->getTemplateCode($template_stack, $options['template']);
            $this->renderer->setElementTemplate($element_template, $options['name']);
        }

        switch ($options['type']) {
            case 'fieldset':
                $fieldset = $this->form->addElement('header', $options['name'], $options['label']);
                if ($options['attributes']) {
                    $fieldset->updateAttributes($options['attributes']);
                }
                break;

            case 'input':
            case 'text':
            case 'email':
            case 'tel':
            case 'date':
            case 'url':
                $field_attributes = $options['attributes'] ?: $this->input_attributes; // pass through class or size etc.
                $this->form->addElement('text', $options['name'], $options['label'], $field_attributes);
                if ($options['type'] != 'input') {
                    $this->{$options['name']}->setType($options['type']);
                    $options['is_'.$options['type']] = true; // e.g. is_email is true
                }
                break;

            case 'textarea':
                $attributes = $options['attributes'];
                // need to work out best way of defining the size of a text area box and standard sizes...
                if ($options['cols'] && $options['rows']) {
                    $attributes .= ' rows="'.$options['rows'].'" cols="'.$options['cols'].'"';
                } elseif (array_key_exists($options['size'], $this->options['textarea_sizes'])) {
                    $size = $options['size'];
                    $attributes .= ' '.$this->options['textarea_sizes'][$size];
                }
                $this->form->addElement('textarea', $options['name'], $options['label'], $attributes);
                break;

            case 'password':
                $field_attributes = $options['attributes'] ?: $this->input_attributes;
                $this->form->addElement('password', $options['name'], $options['label'], $field_attributes);
                if ($options['add_compare']) {
                    $label = $options['compare_label'] ? $options['compare_label'] : 'repeat '.$options['label'];
                    $this->form->addElement('password', 'confirm_'.$options['name'], $label, $field_attributes);
                    $this->form->addRule('confirm_'.$options['name'], $label.' is required', 'required', false, $options['validation']);
                    $this->compare_field_name = 'confirm_'.$options['name'];
                    $this->orginal_field_name = $options['name'];
                    $this->form->addFormRule([$this, '_compareTwoPasswordFields']);
                }
                break;

            case 'checkbox':
                // null here relates to // label output after advcheckbox
                // not used it yet but could be handy at some point...
                // see http://wiki.triangle-solutions.com/index.php/PEAR_HTML_QuickForm#AdvCheckbox


                if (!$options['template'] && $this->options['checkbox_template']) {
                    $element_template = $this->renderer->getTemplateCode('elements', $this->options['checkbox_template']);
                    $this->renderer->setElementTemplate($element_template, $options['name']);
                }

                $attributes = ($options['attributes']) ? $options['attributes'] : $this->checkbox_attributes;
                $value = isset($options['value']) ? $options['value'] : '1';

                $this->form->addElement('advcheckbox', $options['name'], $options['label'], null, $attributes, $value);
                break;

            case 'checkbox_multiple':
                // we might want to allow passing through of a table and which fields to use for id and value
                // previously i passed through ref_data table and then worked out creating the select from there...
                if (!$options['template'] && $this->options['checkbox_template']) {
                    $element_template = $this->renderer->getTemplateCode('elements', $this->options['checkbox_template']);
                    $this->renderer->setElementTemplate($element_template, $options['name']);
                }

                if ($options['values'] && is_array($options['values']) && count($options['values']) > 0) {
                    foreach ($options['values'] as $id => $value) {
                        $el = $options['use_advcheckbox'] ? 'advcheckbox' : 'checkbox';
                        $checkboxes[] = &$this->form->createElement($el, $id, $value);
                    }
                }

                // $options['group_spacer'] relates to the options are split up.... or something.
                if ($options['element_template']) {
                    $this->renderer->setGroupElementTemplate($this->renderer->getTemplateCode('groups', $options['element_template']), $options['name']);
                }

                $append_name = isset($options['append_name']) ? $options['append_name'] : true;
                $group_spacer = $options['group_spacer'] ?: '';
                $this->form->addGroup($checkboxes, $options['name'], $options['label'], $group_spacer, $append_name);
                break;

            case 'radio':
                $radio = [];
                // $options['group_spacer'] relates to the options are split up.... or something.
                if ($options['element_template']) {
                    $this->renderer->setGroupElementTemplate($this->renderer->getTemplateCode('groups', $options['element_template']), $options['name']);
                }

                if ($options['values']) {
                    // values to select have been passed
                    foreach ($options['values'] as $id => $value) {
                        // don't need to name the radio button here as added to a group
                        $radio[] = &$this->form->createElement('radio', null, null, ' '.$value, $id, $this->radio_attributes);
                    }
                    $this->form->addGroup($radio, $options['name'], $options['label'], $options['group_spacer']);
                } else {
                    // assume it is a yes no question
                    $radio[] = &$this->form->createElement('radio', null, null, ' Yes', 1, $this->radio_attributes);
                    $radio[] = &$this->form->createElement('radio', null, null, ' No', 0, $this->radio_attributes);
                    $group_spacer = $options['group_spacer'] ?: '';
                    $this->form->addGroup($radio, $options['name'], $options['label'], $group_spacer);
                }

                break;

            case 'file':
                $field_attributes = $options['attributes'] ?: $this->input_attributes;
                if ($options['max_file_size']) {
                    $this->form->setMaxFileSize($options['max_file_size']);
                }
                $this->form->addElement('file', $options['name'], $options['label'], $field_attributes);
                break;

            case 'select':
                // we might want to allow passing through of a table and which fields to use for id and value
                // previously i passed through ref_data table and then worked out creating the select from there...

                $start_option_text = $options['start_option_text'] ? $options['start_option_text'] : '-- please select --';
                $start = ($options['remove_start_option']) ? [] : ['' => $start_option_text];
                $attributes = $options['attributes'] ?: $this->select_attributes;

                if ($options['values']) {
                    if (is_array($options['values']) && count($options['values']) > 0) {
                        $select_options = $start + $options['values'];
                    } else {
                        $select_options = $start;
                    }

                    $this->form->addElement('select', $options['name'], $options['label'], $select_options, $attributes);
                } elseif ($options['is_numeric'] == true) {
                    if ($options['max_val']) {
                        $options['min_val'] = ($options['min_val']) ? $options['min_val'] : 0;
                        $options['step'] = ($options['step']) ? $options['step'] : 1;

                        if ($options['min_val'] < $options['max_val']) {
                            for ($i = $options['min_val']; $i <= $options['max_val']; $i = $i + $options['step']) {
                                $value = ($options['show_decimals']) ? sprintf("%01.1f", $i) : $i;
                                $select_options[$value] = $value;
                            }
                        } else {
                            for ($i = $options['min_val']; $i >= $options['max_val']; $i = $i - $options['step']) {
                                $value = ($options['show_decimals']) ? sprintf("%01.1f", $i) : $i;
                                $select_options[$value] = $value;
                            }
                        }

                        $select_options = $start + $select_options;
                        $this->form->addElement('select', $options['name'], $options['label'], $select_options, $attributes);
                    }
                } else {
                    // assume it is a yes no question
                    $select_options = $start + ["1" => "Yes", "0" => "No"];
                    $this->form->addElement('select', $options['name'], $options['label'], $select_options, $attributes);
                }
                break;

            case 'hidden':
                $this->form->addElement('hidden', $options['name'], $options['label']);
                break;

            case 'date':
                break;

            case 'html':
                if ($options['template']) {
                    $options['code'] = sprintf($this->renderer->getTemplateCode('html', $options['template']), $options['code']);
                }
                $this->form->addElement('html', $options['code']);
                break;

            default:
                throw new \InvalidArgumentException('Unknown control type: '.$options['type']);
        }

        /**
         * VALIDATIONS...
         */
        if (is_array($options['label'])) {
            $options['label'] = $options['label'][0];
        }

        if ($options['is_required'] || $options['required']) {
            $options['required_message'] = ($options['required_message']) ? $options['required_message'] : $options['label'].' is required';

            if ($options['type'] == 'file') {
                $this->form->addRule($options['name'], $options['required_message'], 'uploadedfile', false, $options['validation']);
            } elseif ($options['type'] == 'checkbox_multiple') {
                $this->form->addGroupRule($options['name'], $options['required_message'], 'required', false, 1);
            } else {
                $this->form->addRule($options['name'], $options['required_message'], 'required', false, $options['validation']);
                $this->form->applyFilter($options['name'], 'trim');
            }
        }

        if ($options['is_email']) {
            $msg = $options['label'].' is not a valid email address';
            $this->form->addRule($options['name'], $msg, 'email', false, $options['validation']);
        }

        if ($options['is_numeric']) {
            $msg = $options['label'].' must be a valid number';
            $this->form->addRule($options['name'], $msg, 'numeric', false, $options['validation']);
        }

        if ($options['exclude_spaces']) {
            $msg = $options['label'].' can only be letters or numbers without spaces';
            $this->form->addRule($options['name'], $msg, 'regex', '/^[a-zA-Z_0-9\-]+$/', $options['validation']);
        }

        if ($options['regex']) {
            $options['regex_message'] = $options['regex_message'] ?: $options['label'].' has an invalid format';
            $this->form->addRule($options['name'], $options['regex_message'], 'regex', $options['regex'], $options['validation']);
        }

        if ($options['has_min_chars']) {
            $msg = $options['label'].' is too short (must be at least '.$options['has_min_chars'].' characters)';
            $this->form->addRule($options['name'], $msg, 'minlength', $options['has_min_chars'], $options['validation']);
        }

        if ($options['has_max_chars']) {
            $msg = $options['label'].' is too long (must be no more than '.$options['has_max_chars'].' characters)';
            $this->form->addRule($options['name'], $msg, 'maxlength', $options['has_max_chars'], $options['validation']);
        }

        if ($options['do_freeze']) {
            $this->form->freeze($options['name']);
        }
    }

    /**
     * add any errors from $this->form->_errors into standard error class..
     */
    public function getErrors()
    {
        return $this->form->_errors;
    }

    /**
     * check to see if a form validates
     */
    public function isValid()
    {
        if ($this->form->isSubmitted() && $this->form->validate()) {
            return true;
        } elseif ($this->form->isSubmitted()) {
            return false;
        }

        return false;
    }

    /**
     * check to see if a form has been submitted
     */
    public function isSubmitted()
    {
        return ($this->form->isSubmitted()) ? true : false;
    }

    /**
     * Return the filled in values of forms
     *
     * @param bool $get_files whether to also return uploaded files
     * @return array
     */
    public function getSubmitValues($get_files = false)
    {
        return $this->form->getSubmitValues($get_files);
    }

    /**
     * Set the default values for a form
     *
     * @param array $defaults
     * @throws \HTML_QuickForm_Error
     */
    public function setDefaults(array $defaults = [])
    {
        $this->form->setDefaults($defaults);
    }

    /**
     * Set the constant values for a form
     *
     * @param array $constants
     * @throws \HTML_QuickForm_Error
     */
    public function setConstants(array $constants = [])
    {
        $this->form->setConstants($constants);
    }

    /**
     * Returns the value of an uploaded file
     *
     * @param string name of file input
     * @return array I think
     */
    public function getFile($name)
    {
        if ($f = $this->form->getElement($name)) {
            $value = $f->getValue();
            if (is_uploaded_file($value['tmp_name'])) {
                return $value;
            }
        }

        return [];
    }

    public function __call($func, $args = [])
    {
        return call_user_func_array([$this->form, $func], $args);
    }

    function __isset($name)
    {
        return (
            $this->form->elementExists($name) ||
            method_exists($this->form, $name) ||
            !!($this->form->getAttributes($name))
        );
    }

    public function __get($k)
    {
        if ($this->form->elementExists($k)) {
            $element = $this->form->getElement($k);
            if (!$element->getAttribute('id')) {
                $element->setAttribute('id', $element->getAttribute('name'));
            }

            return $element;
        } elseif ($attribute = $this->form->getAttribute($k)) {
            return $attribute;
        } elseif (method_exists($this->form, $k)) {
            return call_user_func([$this->form, $k]);
        }

        return $this->form->$k;
    }

    public function __toString()
    {
        return $this->toHtml();
    }

    public function offsetExists($index)
    {
        $element = $this->{$index};
        return ($element instanceof \HTML_QuickForm_element);
    }

    public function offsetGet($name)
    {
        $element = $this->{$name};
        if ($element instanceof \HTML_QuickForm_element) {
            return $element->getValue();
        } else {
            throw new \Exception('Unknown form field');
        }
    }

    public function offsetSet($name, $value)
    {
        $element = $this->{$name};
        if ($element instanceof \HTML_QuickForm_element) {
            return $element->setValue($value);
        } else {
            throw new \Exception('Unknown form field');
        }
    }

    /**
     * @param array $fields
     * @return array|bool
     */
    public function _compareTwoPasswordFields(array $fields)
    {
        $compare = $this->compare_field_name;
        $orig = $this->orginal_field_name;
        if ($fields[$orig] != $fields[$compare]) {
            return ['Passwords do not match'];
        }

        return true;
    }

    /**
     * Checks if a field exists before adding the rule
     */
    public function addRule()
    {
        $args = func_get_args();

        if (is_callable($args[0])) {
            return call_user_func_array([$this->form, 'addFormRule'], $args);
        }

        $fields = (array)$args[0];

        foreach ($fields as $field) {
            if (!$this->form->elementExists($field)) {
                continue;
            }
            call_user_func_array([$this->form, 'addRule'], $args);
        }
    }

}
